import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/**
 * Clase que representa y guarda toda la informacion del virus,
 * Tamano, direccion, posicion y estado actual
 * Ademas contiene el metodo para dibujar el mismo y moverlo segun los limites de un Rectangle2D
 *
 * @author Ricardo Montserrat
 * @version 11/04/2020
 */
public class Virus
{
    public boolean isAlive = true;
    private double x = 0;
    private double y = 0;
    private double directionX = 1;
    private double directionY = 1;
    private final double RADIUS = 30;
    private final byte DELAYSEC;
    private final Image VIRUS_IMG = new ImageIcon("virus.png").getImage();


    public Virus(byte DELAYSEC) { this.DELAYSEC = DELAYSEC; }

    public void moveVirus(Rectangle2D limits)
    {
        double width = limits.getWidth();
        double height = limits.getHeight();
        x += x + RADIUS / 2 > width || x + RADIUS / 2 < 0 ? directionX = -directionX : directionX;
        y += y + RADIUS / 2 > height || y + RADIUS / 2 < 0 ? directionY = -directionY : directionY;
    }

    public void setRandomPosition(Rectangle2D limits)
    {
        x = Math.random() * limits.getWidth();
        y = Math.random() * limits.getHeight();
    }

    public void drawVirus(Graphics2D graphics)
    {
        if (isAlive) graphics.drawImage(VIRUS_IMG, (int) x, (int) y, (int) RADIUS, (int) RADIUS, null);
    }

    public byte getDelaySec() {return DELAYSEC; }

    public double getX() { return x; }

    public double getY() { return y; }

    public double getRadius() { return RADIUS; }
}
