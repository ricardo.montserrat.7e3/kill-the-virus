import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Setup de la gui de la aplicacion.
 * Con un boton que contiene la funcion de anadir viruses de cantidad aleatoria entre 5-10, al panel
 * e iniciar el thread de los mismos
 *
 * @author Ricardo Montserrat
 * @version 11/04/2020
 */
public class KillTheVirusGUI extends JFrame
{
    private final PanelVirus PANEL_VIRUSES = new PanelVirus();

    public KillTheVirusGUI()
    {
        setBounds(600, 300, 400, 350);
        setTitle("Kill The Virus");
        add(PANEL_VIRUSES, BorderLayout.CENTER);
        JPanel botonera = new JPanel();
        JButton boto1 = new JButton("Pandemic!");
        JButton boto2 = new JButton("End");
        botonera.add(boto1);
        botonera.add(boto2);
        boto1.addActionListener(new KillTheVirusGUI.ClickButtonGoVirus());
        boto2.addActionListener((x) -> System.exit(0));
        add(botonera, BorderLayout.SOUTH);
    }

    class ClickButtonGoVirus implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent actionEvent)
        {
            byte virusLength = (byte) (5 + (Math.random() * 5 + 1));
            for (byte i = 0; i < virusLength; i++)
            {
                Virus virus = new Virus((byte) ((Math.random() * 5 + 2) * 5));
                virus.setRandomPosition(PANEL_VIRUSES.getBounds());
                PANEL_VIRUSES.add(virus);
                new Thread(new VirusRunnable(virus, PANEL_VIRUSES)).start();
            }
        }

    }
}
