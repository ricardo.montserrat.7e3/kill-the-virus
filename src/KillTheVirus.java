import javax.swing.*;

/**
 * Clase que crea una gui, en la cual puedes invocar un grupo de viruses que rebotan por toda la ventana
 * los cuales puedes clickear para destruirlos. Estos trabajan mediante threads que calculan el movimiento
 * y los mismos dentro de un unico panel el cual los dibuja en pantalla una y otra vez hasta que mueran.
 *
 * @author Ricardo Montserrat
 * @version 11/4/2020
 */
public class KillTheVirus
{
    public static void main(String[] args)
    {
        JFrame game = new KillTheVirusGUI();
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game.setVisible(true);
    }
}
