import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

/**
 * Panel contenidor de un array de Viruses.
 * Dibuja los virus de acuerdo a su posicion dentro de si mismo.
 * Implementa el mouse listener para detectar el click y calcular la distancia entre el virus y el raton.
 *
 * @author Ricardo Montserrat
 * @version 11/04/2020
 */
public class PanelVirus extends JPanel implements MouseListener
{
    private final ArrayList<Virus> viruses;

    public PanelVirus()
    {
        viruses = new ArrayList<>();
        addMouseListener(this);
    }

    public void add(Virus virus) { viruses.add(virus); }

    public void paint(Graphics g)
    {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        for (Virus virus : viruses)
        {
            virus.drawVirus(g2);
        }
        Toolkit.getDefaultToolkit().sync();
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        for (Virus virus : viruses)
        {
            if (virus.isAlive)
            {
                int x = e.getX(), y = e.getY();
                int xVirus = (int) virus.getX(), yVirus = (int) virus.getY();

                double squaredCoordinates = (x - xVirus) * (x - xVirus) + (y - yVirus) * (y - yVirus);
                virus.isAlive = !(squaredCoordinates <= (virus.getRadius()) * (virus.getRadius()));
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) { }
    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
}
