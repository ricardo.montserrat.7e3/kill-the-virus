/**
 * Metodo con el que trabaja el thread del virus
 * Llama la funcion de mover del virus, espera unos milisegundos
 * y llama al panel para que repinte la gui con la nueva posicion del virus
 */
public class VirusRunnable implements Runnable
{
    private final Virus VIRUS;
    private final PanelVirus PANEL;

    public VirusRunnable(Virus virus, PanelVirus pan)
    {
        this.VIRUS = virus;
        PANEL = pan;
    }

    @Override
    public void run()
    {
        while (VIRUS.isAlive)
        {
            VIRUS.moveVirus(PANEL.getBounds());
            try { Thread.sleep(VIRUS.getDelaySec()); }
            catch (Exception e) { System.out.println(e.toString());}
            PANEL.repaint();
        }
    }
}
